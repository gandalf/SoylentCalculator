# SoylentCalculator
## run

1. Besorge dir ampl von: https://ampl.com/try-ampl/download-a-free-demo/#linux
1. Starte in der Konsole 
	ampl.linux64/ampl
2. Eingabe 
	include main.ampl;
3. Es werden die Einkaufsmengen in g bzw. Stück (bei Vitaminpillen), die Gesamtmasse in g und der Kaufpreis in Euro jeweiln für eine Tagesration angezeigt.
 Andere berechnete Werte können mit 
	display variable;
angezeigt werden.

## neue Zutaten eintragen

1. Es wird immer die Datei Rezept-pool.ods geändert.
  * neue Zutaten müssen manuell mit exakt gleichem Namen in die Tabellen "data\_raw" und "preise", sowie in die Datei "zutaten.dat" eingetragen werden.
  * Nährwerte manuell in "data\_raw" eintragen, sie werden automatisch in "Nährwerte bereinigt" übertragen.
  * evtl. muss die Anzahl von übertragenen Zeilen in letzterer Tabelle angepasst werden.
2. Die Tabelle "Nährwerte bereinigt" als "werte.csv" abspeichern, mit **Leerzeichen** als Spaltentrenner.
Ebenso die Tabelle "Preise" als "preise.csv".
3. Entferne in den genannten csv-Dateien die erste Zeile und ändere die Dateiendung in .dat
4. Führe "sed -i -r -e  s/\<[0-9\.]*/0/g werte.dat" aus
4. Jetzt müsste das Programm wie oben laufen.

## Neue Constraints, zu berücksichtigende Nährwerte

Passiert über Änderungen in verschiedenen Dateien, u.a. Rezept.mod. Nicht ganz trivial, werde ich vielleicht später
weiter ausführen.

## rumspielen
* Andere Zutaten berücksichtigen: Kommentiere Zeilen in zutaten.dat aus.
* Andere Nährwerte: Zeilen in grenzen-working.dat auskommentieren und sicherstellen, dass in grenzen.dat die entsprechende
Zeile aktiv ist. Die Datei grenzen-orig.dat sollte nicht verändert werden.
* Wenn die Datei grenzen-orig.dat aktiv ist und in zutaten.dat nur die Zeilen mit *OR aktiv sind, erhält das Programm
genau die Eingabedaten des ursprünglichen Rezepts. 
* Für diverse Strafkosten: in config.dat rumspielen. Dort die Kommentare beachten.

