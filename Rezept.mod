set ZUTATEN;
set STOFFE;
set INDEX; #Indexmenge
param preis {z in ZUTATEN} >= 0; #in €/kg oder €/10Stück
param werte {z in ZUTATEN, s in STOFFE} >= 0; #je 100g oder Stück
param minwert {s in STOFFE} >= 0 default 0;   #ebenso
param maxwert {s in STOFFE} >= 0 default Infinity; #ebenso
var menge {z in ZUTATEN} >= 0; # in g oder Stück
param ref {z in ZUTATEN} >= 0 default 0;

param bezugsmenge {z in ZUTATEN} default 100; #nicht immer ist menge in 100g, teilweise in Stück
param weight_penalty >= 0 <= 1 default 0.05;       #Transportkosten je 100g
param pill_penalty {z in ZUTATEN, i in INDEX} default 0; #für einzelne Zutaten können Strafkosten bei Überschreiten einer Höchstmenge verhängt werden
#Die Höchstmenge wird in pill_penalty[z,1], die Strafkosten für jede darüberliegende Einheit in pill_penalty[z,2] gespeichert.

var kaufpreis = sum {z in ZUTATEN} preis[z]/(10*bezugsmenge[z])*menge[z]; #Alnatura-Preise sind in €/1kg statt 100g…
var omega_ratio = sum{z in ZUTATEN} menge[z]*werte[z,"Omega-6"]/sum{y in ZUTATEN} menge[y]*werte[z,"Omega-3"];
var rezeptwert {s in STOFFE} = sum {z in ZUTATEN} menge[z]*werte[z,s]/bezugsmenge[z];
var total_weight = sum {z in ZUTATEN} menge[z];
var total_pill_penalty = sum {z in ZUTATEN} max(menge[z]-pill_penalty[z,1],0)*pill_penalty[z,2];
#Wenn die zutatenweise Obergrenze in pill_penalty[z,1] überschritten ist, werden Strafkosten für die Überschussmenge berechnet.

minimize gesamtpreis: kaufpreis*(1-weight_penalty) + total_weight/100*weight_penalty + total_pill_penalty; 
subject to ugrenze {s in STOFFE}: rezeptwert[s] >= minwert[s]; #undefined renders 0 => no problem here
subject to ogrenze {s in STOFFE}: rezeptwert[s] <= maxwert[s]; #undefined should render Infinity here.
#subject to omega_ratio_constraint: omega_ratio <= 6;

